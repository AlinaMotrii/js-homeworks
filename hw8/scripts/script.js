const pageText = document.getElementsByTagName("p");
console.log(pageText);
const pageTextArr = Array.from(pageText); //превращаем в массив
pageTextArr.forEach((elem) => {
  //перебираем массив
  elem.style.backgroundColor = "#ff0000";
});

const elemList = document.getElementById("optionsList");
console.log(elemList);
const elemListParent = elemList.parentNode;
console.log("elemListParent-->", elemListParent);
elemList.childNodes.forEach((elem) => {
  console.log(elem.nodeName, elem.nodeType);
});

document.getElementById("testParagraph").textContent = "This is a paragraph";

const header = document.querySelector(".main-header");
console.log(header.children);
const headerElementsArr = Array.from(header.children);
headerElementsArr.forEach((elem) => {
  elem.classList.add("nav-item");
});

const title = document.getElementsByClassName("section-title");
const titleArr = Array.from(title);
titleArr.forEach((elem) => {
  elem.classList.remove("section-title");
});
