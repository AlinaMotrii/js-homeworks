const buttons = document.querySelectorAll(".btn");
document.addEventListener("keydown", changeColor);
function changeColor(e) {
  buttons.forEach((button) => {
    button.classList.remove("color-blue");
    if (e.code === button.getAttribute("data-key")) {
      button.classList.add("color-blue");
    }
  });
}
