function filterBy(arr, dataType) {
  const newArr = [];
  arr.forEach(function (element) {
    if (typeof element !== dataType) {
      newArr.push(element);
    }
  });
  return newArr;
}
const arr = ["jhjh", 67, null]
const newArr = filterBy(arr, 'string')


const divs = document.querySelectorAll(".test-div");
const divsArr = Array.from(divs);

const btn = document.querySelector(".btn");

// window.addEventListener("keydown", (e) => console.log(e.key, "press"));

btn.addEventListener("mouseout", () => alert("Bay!"));
btn.addEventListener("mouseover", () => alert("Over btn!"));

const input = document.querySelector(".input");

input.addEventListener("focus", (e) => console.log(e.target.value));
input.addEventListener('blur', function() {
    if (!this.value.includes('@')) { // це не електронна адреса
      // показати помилку
      this.classList.add("error");
      // ...та повернути фокус
      input.focus();
    } else {
      this.classList.remove("error");
    }
  };)

// const user = {
//   name: true,
// };

// if (user.name) {
//   alert("Hello " + user.name);
// }

// window.addEventListener("scroll", function () {
//   document.getElementById("showScroll").innerHTML = pageYOffset + "px";
// });
