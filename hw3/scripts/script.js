let userNumber = prompt("Please, enter your number", "your number");
while (!userNumber || isNaN(userNumber)) {
  userNumber = prompt("Please, try again");
}

for (let i = 5; i <= userNumber; i = i + 5) {
  console.log(i);
}

// або такий варіант циклу
// for (let i = 0; i <= userNumber; i++) {
//   if (i % 5 === 0) {
//     console.log(i);
//   }
// }

if (userNumber < 5) {
  alert("Sorry, no numbers");
}
