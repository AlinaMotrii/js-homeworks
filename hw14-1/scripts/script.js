const pageWrapper = document.querySelector(".page-wrapper");
const themeBtn = document.querySelector(".theme-btn");
if (localStorage.getItem("theme") === "dark") {
  pageWrapper.classList.add("dark");
}
function changeTheme(element) {
  if (!element.classList.contains("dark")) {
    element.classList.add("dark");
    localStorage.setItem("theme", "dark");
  } else {
    element.classList.remove("dark");
    localStorage.setItem("theme", "light");
  }
}

themeBtn.addEventListener("click", () => changeTheme(pageWrapper));
