const images = document.querySelectorAll(".image-to-show");
let i = 1;
function startSlideShow() {
  return setInterval(function () {
    images.forEach((image) => {
      image.classList.remove("active");
    });
    if (i === images.length) {
      //проверяем равен ли i четырем. если да - присваеваем i ноль.
      i = 0;
    }
    images[i].classList.add("active");
    i++;
  }, 3000);
}
const slideShow = startSlideShow();
const buttonStop = document.createElement("button");
buttonStop.innerHTML = "Припинити!";
document.querySelector(".images-wrapper").appendChild(buttonStop);
buttonStop.addEventListener("click", stopImages);
function stopImages(e) {
  clearInterval(slideShow);
}
const buttonRestart = document.createElement("button");
buttonRestart.innerHTML = "Відновити показ";
document.querySelector(".images-wrapper").appendChild(buttonRestart);
buttonRestart.addEventListener("click", startSlideShow);

