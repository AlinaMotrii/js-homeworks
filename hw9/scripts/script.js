let someArr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function listItems(arr, parent = document.body) {
  const list = document.createElement("ul");
  arr.forEach((element) => {
    const li = document.createElement("li");
    li.innerHTML = element;
    list.append(li);
  });
  parent.append(list);
}

listItems(someArr, document.body);
