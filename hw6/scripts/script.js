function createNewUser() {
  const firstName = prompt("Please enter your first name");
  const lastName = prompt("Please enter your last name");
  const birthday = prompt("Please, enter your birthday dd.mm.yyyy");
  const newUser = {
    firstName: firstName,
    lastName: lastName,
    birthday: birthday.split(".").reverse().join("."),
    getLogin: function () {
      const initials = this.firstName[0] + this.lastName;
      const result = initials.toLowerCase();
      return result;
    },
    getAge: function () {
      let age =
        (new Date().getTime() - new Date(this.birthday).getTime()) /
        (365.242199 * 24 * 60 * 60 * 1000);
      return Math.floor(age);
    },

    getPassword: function () {
      const password =
        this.firstName[0].toUpperCase() + new Date(this.birthday).getFullYear();
      return password;
    },
  };
  return newUser;
}

const user = createNewUser();
console.log(user.getAge());
console.log(user.getPassword());

