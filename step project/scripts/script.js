// количество изображений в контейнере
const allImages = 12;
// имена папок с изображениями
const imagesFolderName = [
  "graphic-design",
  "web-design",
  "landing-page",
  "wordpress",
];
// функция создает картинки из заданной папки - папка передается в качестве параметра imgFolder
function showImages(imgFolder) {
  const loadBtn = document.querySelector(".load-btn");
  if (loadBtn.classList.contains("hidden")) {
    loadBtn.classList.remove("hidden");
  }
  const wrapper = document.querySelector(".images-container");
  wrapper.innerHTML = "";
  for (let i = 1; i <= allImages; i++) {
    const image = document.createElement("img");
    image.src = `./images/${imgFolder}/work-image${i}.jpg`;
    image.classList.add("service-image");

    const imgWrapper = document.createElement("div");
    imgWrapper.classList.add("image-wrapper");
    const imgCard = document.createElement("div");
    imgCard.classList.add("work-card");
    imgCard.innerHTML = `
              <div class="work-card-links">
               <a class="card-link link" href="#"></a>
               <a class="card-link square" href="#"></a>
              </div>
              <span class="work-card-content"> creative design</span> 
              <span class="work-card-text">Web Design</span>`;
    imgWrapper.append(image);
    imgWrapper.append(imgCard);

    wrapper.append(imgWrapper);
  }
}
// создает картинки, которые рандомно достаются из всех папок
function showAllImages() {
  const loadBtn = document.querySelector(".load-btn");
  if (loadBtn.classList.contains("hidden")) {
    loadBtn.classList.remove("hidden");
  }
  const wrapper = document.querySelector(".images-container");
  wrapper.innerHTML = "";
  for (let i = 1; i <= allImages; i++) {
    const image = document.createElement("img");
    image.src = `./images/${
      imagesFolderName[Math.floor(Math.random() * 4)]
    }/work-image${i}.jpg`;
    image.classList.add("service-image");
    const imgWrapper = document.createElement("div");
    imgWrapper.classList.add("image-wrapper");
    const imgCard = document.createElement("div");
    imgCard.classList.add("work-card");
    imgCard.innerHTML = `
              <div class="work-card-links">
               <a class="card-link link" href="#"></a>
               <a class="card-link square" href="#"></a>
              </div>
              <span class="work-card-content"> creative design</span> 
              <span class="work-card-text">Web Design</span>`;
    imgWrapper.append(image);
    imgWrapper.append(imgCard);

    wrapper.append(imgWrapper);
  }
}
// вызываем функцию, чтобы картинки отображались сразу при загрузке страницы
showAllImages();
// перебираем табы. получаем фтрибут data-type каждой табы. если имя data-type "all",то при клике запускается функция showAllImages, если другое- функция showImages. в первом случае отображатся рандомно картинки  из всех папок,во втором- из конкретной
const tabs = document.querySelectorAll(".work-list-item");
tabs.forEach(function (item) {
  let dataTab = item.getAttribute("data-tab");

  if (dataTab === "all") {
    item.addEventListener("click", () => showAllImages());
  } else {
    item.addEventListener("click", () => showImages(dataTab));
  }
});

function loadMoreImages(e) {
  const wrapper = document.querySelector(".images-container");
  for (let i = 1; i <= allImages; i++) {
    const image = document.createElement("img");
    image.src = `./images/${
      imagesFolderName[Math.floor(Math.random() * 4)]
    }/work-image${i}.jpg`;
    image.classList.add("service-image");
    const imgWrapper = document.createElement("div");
    imgWrapper.classList.add("image-wrapper");
    const imgCard = document.createElement("div");
    imgCard.classList.add("work-card");
    imgCard.innerHTML = `
              <div class="work-card-links">
               <a class="card-link link" href="#"></a>
               <a class="card-link square" href="#"></a>
              </div>
              <span class="work-card-content"> creative design</span> 
              <span class="work-card-text">Web Design</span>`;
    imgWrapper.append(image);
    imgWrapper.append(imgCard);

    wrapper.append(imgWrapper);
  }
  e.target.classList.add("hidden");
}

const loadBtn = document.querySelector(".load-btn");
loadBtn.addEventListener("click", loadMoreImages);

const tabsItems = document.querySelectorAll(".tabs-item");
const tabsServices = document.querySelectorAll(".tabs-services");
tabsItems.forEach(function (item) {
  item.addEventListener("click", function () {
    let titleTab = item;
    let tabId = titleTab.getAttribute("data-tab");
    let contentTab = document.getElementById(tabId);

    tabsItems.forEach(function (item) {
      if (item.classList.contains("is-active")) {
        item.classList.remove("is-active");
      }
    });

    tabsServices.forEach(function (item) {
      if (item.classList.contains("is-active")) {
        item.classList.remove("is-active");
      }
    });
    titleTab.classList.add("is-active");
    contentTab.classList.add("is-active");
  });
});

// What People Say About theHam
function chooseReview(customer) {
  const customerTab = customer;
  const customerId = customerTab.getAttribute("data-customer");
  const customerContent = document.getElementById(customerId);

  customerList.forEach(function (customer) {
    if (customer.classList.contains("active")) {
      customer.classList.remove("active");
    }
  });

  customerInfo.forEach(function (customer) {
    if (customer.classList.contains("active")) {
      customer.classList.remove("active");
    }
  });
  customerTab.classList.add("active");
  customerContent.classList.add("active");
}

const customerList = document.querySelectorAll(".customer-list-item");
const customerInfo = document.querySelectorAll(".customer-info");
customerList.forEach(function (customer) {
  customer.addEventListener("click", () => chooseReview(customer));
});

const galleryBtnRight = document.querySelector(".gallery-btn.right");
const galleryBtnLeft = document.querySelector(".gallery-btn.left");

function moveRight() {
  let active;
  customerList.forEach(function (customer, index) {
    if (index < customerList.length - 1) {
      if (customer.classList.contains("active")) {
        customer.classList.remove("active");
        active = index + 1;
      }
    } else {
      if (customer.classList.contains("active")) {
        customer.classList.remove("active");
        active = 0;
      }
    }
  });
  customerList[active].classList.add("active");
  const customerId = customerList[active].getAttribute("data-customer");
  customerInfo.forEach(function (customer) {
    customer.classList.remove("active");
  });
  const customerContent = document.getElementById(customerId);
  customerContent.classList.add("active");
}
galleryBtnRight.addEventListener("click", moveRight);

function moveLeft() {
  let active;
  customerList.forEach(function (customer, index) {
    if (index === 0) {
      if (customer.classList.contains("active")) {
        customer.classList.remove("active");
        active = customerList.length - 1;
      }
    } else {
      if (customer.classList.contains("active")) {
        customer.classList.remove("active");
        active = index - 1;
      }
    }
  });
  customerList[active].classList.add("active");
  const customerId = customerList[active].getAttribute("data-customer");
  customerInfo.forEach(function (customer) {
    customer.classList.remove("active");
  });
  const customerContent = document.getElementById(customerId);
  customerContent.classList.add("active");
}
galleryBtnLeft.addEventListener("click", moveLeft);
