function createNewUser() {
  const firstName = prompt("Please enter your first name");
  const lastName = prompt("Please enter your last name");
  const newUser = {
    firstName: firstName,
    lastName: lastName,
    getLogin: function () {
      const initials = this.firstName[0] + this.lastName;
      const result = initials.toLowerCase();
      return result;
    },
  };
  return newUser;
}

const user = createNewUser();
console.log(user.getLogin());

