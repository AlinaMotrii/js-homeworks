const visibilityBtn1 = document.getElementById("visibilityBtn1");
const visibilityBtn2 = document.getElementById("visibilityBtn2");
const passwordInput1 = document.getElementById("password-1");
const passwordInput2 = document.getElementById("password-2");
visibilityBtn1.addEventListener("click", () =>
  toggleVisability(visibilityBtn1, passwordInput1)
);
visibilityBtn2.addEventListener("click", () =>
  toggleVisability(visibilityBtn2, passwordInput2)
);
function toggleVisability(btn, input) {
  btn.classList.toggle("fa-eye");
  btn.classList.toggle("fa-eye-slash");
  if (input.type === "password") {
    input.type = "text";
  } else {
    input.type = "password";
  }
}

const form = document.querySelector(".password-form");
form.addEventListener("submit", formSubmit);
function formSubmit(e) {
  e.preventDefault();
  const errorMessage = document.querySelector("#error");
  if (passwordInput1.value === passwordInput2.value) {
    errorMessage.classList.remove("active");
    setTimeout(() => alert("You are welcome"), 100);
  } else {
    errorMessage.classList.add("active");
  }
}
